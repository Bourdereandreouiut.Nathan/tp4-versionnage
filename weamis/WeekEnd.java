import java.util.List;
import java.util.ArrayList;
public class WeekEnd{
    private List<Personne> listeAmis;
    private List<Depense> listeDepenses;
    public WeekEnd(){
        this.listeAmis = new ArrayList<Personne>();
        this.listeDepenses = new ArrayList<Depense>();
    }
    public void ajouterPersonne(Personne personne){
        this.listeAmis.add(personne);
    }
    public void ajouterDepense(Depense dpense){
        this.listeDepenses.add(dpense);
    }
    public Personne estPersonne(Personne personne){
        Personne lapersonne = null;
        if (this.listeAmis.contains(personne)){
            for (Personne personnee:this.listeAmis){
                if (personnee.equals(personne)){
                    return personnee;
                }
            }
        }
        return lapersonne;
    }
    // totalDepensesPersonne prend paramètre une personne
    // et renvoie la somme des dépenses de cette personne.
    public int depenseParPersonne(Personne personne){
        int res = 0;
        for (Depense depense:this.listeDepenses){
            if (depense.getPersonne().equals(personne)){
                res+=depense.getmontant();
            }
        }
        return res;
        
    }
    // totalDepenses renvoie la somme de toutes les dépenses.
    public int totalDepenses(){
        int res = 0;
        for (Depense depense:this.listeDepenses){
                res+=depense.getmontant();
        }
        return res;
        
    }
    // depenseMoyenne renvoie la moyenne des dépenses par
    // personne
    public double depenseMoyenne(){
       double res = 0.0;
       for (Depense dep: this.listeDepenses){
           res+= dep.getmontant();
       } 
        return res/this.listeDepenses.size();
    }
    // totalDepenseProduit prend un nom de produit en paramètre et renvoie la
    // somme des dépenses pour ce produit.
    // (du pain peut avoir été acheté plusieurs fois...)
    public int totalDepenseProduit(String nomProduit){
        int res = 0;
        for (Depense dep:this.listeDepenses){
            if (dep.getproduit().equals(nomProduit)){
                res += dep.getmontant();
            }
        }
        return res;
    }
    // avoirParPersonne prend en paramètre une personne et renvoie
    // son avoir pour le week end.
    public double avoirPersonne(Personne personne){
        Double res = 0.0;
        res = this.depenseParPersonne(personne)-depenseMoyenne();
        return res;
    }
    @Override
    public String toString(){
        String res = "";
        res += "Amis : "+this.listeAmis +"\n"+ "Depenses : " +this.listeDepenses;
        return res;
    }
    }
