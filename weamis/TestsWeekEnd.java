package weamis;
import org.junit.*;
import static org.junit.Assert.assertEquals;


public class TestsWeekEnd{
    Personne jean = new Personne("Jean");
    Depense depJ = new Depense("Shampoo", 17, jean);
    Personne paul = new Personne("Paul");
    Depense depP = new Depense("T-Shirt", 74, paul);
    WeekEnd we = new WeekEnd();
    @Test
    public void testajouterPersonneDepense(){
        we.ajouterPersonne(jean);
        we.ajouterDepense(depJ);
        assertSame(jean,we.estPersonne(jean));
    }
    @Test
    public void testDepenseParPersonne(){
        we.ajouterPersonne(paul);
        we.ajouterDepense(depJ);
        int var = we.depenseParPersonne();
        assertEquals(17,var);
    }
    @Test
    public void testTotalDepense(){
        int varr = we.totalDepenses();
        assertEquals(17+74,varr);
    }
    @Test
    public void testDepenseMoyenne(){
        double moy = we.depenseMoyenne();
        double lamoy = 17.0+74.0;
        lamoy /= 2.0;
        assertEquals(moy,lamoy);
    }
    @Test
    public void testTotalDepenseProduit(){
    int depProd = we.totalDepenseProduit("T-Shirt");
    assertEquals(depProd,74); 
    }
    @Test
    public void testAvoirPersonne(){
        Double res = we.avoirPersonne(jean);
        double moy = we.depenseMoyenne();
        assertEquals(res,17.0-moy);
    }
    @Test
    public void testAucuneDepense(){
        Personne benjamin = new Personne("Benjamin");
        Personne anne = new Personne("Anne");
        WeekEnd wee = new WeekEnd();
        wee.ajouterPersonne(benjamin);
        wee.ajouterPersonne(anne);
        double avoirA = wee.avoirPersonne(anne);
        double avoirB = wee.avoirPersonne(benjamin);
        assertEquals(0,avoirB);
        assertEquals(avoirA,avoirB);
        
    }
    
}