public class ExecutableWE{
    public static void main(String[] args){
        Personne jean = new Personne("Jean");
        Personne pierre = new Personne("Pierre");
        Personne anne = new Personne("Anne");
        Personne paul = new Personne("Paul");
        Depense depj = new Depense("Shampoo", 17, jean);
        Depense deppi = new Depense("truc",23,pierre);
        Depense deppaul = new Depense("chose",37,paul);
        Depense depje = new Depense("machin",63,jean);
        Depense deppaull = new Depense("machin",63,paul);
        WeekEnd WE = new WeekEnd();
        WE.ajouterPersonne(jean);
        WE.ajouterPersonne(pierre);
        WE.ajouterPersonne(anne);
        WE.ajouterPersonne(paul);
        WE.ajouterDepense(depj);
        WE.ajouterDepense(deppi);
        WE.ajouterDepense(deppaul);
        WE.ajouterDepense(depje);
        WE.ajouterDepense(deppaull);
        System.out.println(WE);
        System.out.println(WE.depenseParPersonne(anne));
        System.out.println(WE.depenseParPersonne(jean));
        System.out.println(WE.depenseMoyenne());
        System.out.println(WE.avoirPersonne(anne));
        System.out.println(WE.avoirPersonne(jean));
        System.out.println(WE.totalDepenseProduit("machin"));
    }
}